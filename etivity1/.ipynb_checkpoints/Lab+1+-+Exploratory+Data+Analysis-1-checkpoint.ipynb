{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lab 1: Exploratory Data Analysis (EDA)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This lab exercise demonstrates EDA of an example dataset with the Python modules `Pandas`, `Numpy`, `Matplotib` and `Seaborn`.\n",
    "\n",
    "> \"At a high level, EDA is the practice of using visual and quantitative methods to understand and summarize a dataset without making any assumptions about its contents. It is a crucial step to take before diving into machine learning or statistical modeling because it provides the context needed to develop an appropriate model for the problem at hand and to correctly interpret its results.\"\n",
    "> <a href=\"https://www.svds.com/value-exploratory-data-analysis/\">Chloe Mawer</a> "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A. Importing Modules and Dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The Python modules uses in this tutorial are:\n",
    "\n",
    "# - Pandas\n",
    "# - Numpy\n",
    "# - Matplotlib\n",
    "# - Seaborn\n",
    "\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import seaborn as sns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After importing the modules, the next step is to load the dataset into a Pandas dataframe.\n",
    "Dataframe is the term used in Pandas for two-dimensional arrays.\n",
    "\n",
    "In this tutorial, we use the example dataset loans_train.csv available as a CSV file.\n",
    "The file loans_train.csv is expected to be in the same directory as this notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv('./loans_train.csv')\n",
    "\n",
    "# You can also open loans_train.csv with Excel and take a look at it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## B. Quick Data Exploration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the dataset is loaded into a dataframe, we can have a look at the first and the last few data rows with the dataframe methods `head()` and `tail()`, respectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print first 5 rows of the dataframe\n",
    "df.head(5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print last 5 rows of the dataframe\n",
    "df.tail(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that the dataset has 614 data rows, and both numerical and categorical attributes. The data rows are typically called *observations* or *examples*, and the columns are also called *attributes* or *features*.\n",
    "\n",
    "The last attribute `Loan_Status` indicates whether a loan was granted (Y) or not (N). This attribute would be a primary candidate for an attribute to *learn* to predict from the values of the other attributes. Before building a predictive model, though, the first step is to get to know the dataset better (the goal of this exercise) and prepare it for machine learning (the goal of the next exercise)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### B.1. Quick Examination of Numerical Attributes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print statistical summary for all numerical attributes\n",
    "df.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a few inferences we can draw by looking at the output of `describe()`:\n",
    "\n",
    "- `LoanAmount` has (614 – 592) 22 missing values.\n",
    "- `Loan_Amount_Term` has (614 – 600) 14 missing values.\n",
    "- `Credit_History` has (614 – 564) 50 missing values.\n",
    "\n",
    "Note that `Credit_History` is rather a categorical attribute despite having numerical values. We can see that about 84% of the applicants have a credit history. How? `Credit_History` has value 1 for those who have a credit history, and 0 otherwise. The mean of `Credit_History` is 0.84, i.e. 84% of the applicants have a credit history.\n",
    "\n",
    "Please note that we can also get an idea of a possible skew in the data by comparing the mean to the median. The median of each numerical attribute is the 50% figure."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also examine the correlation matrix of all numerical attributes in the form of a **heat map**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#correlation matrix\n",
    "sns.heatmap(df.corr());"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The heat map suggests correlation greater than 0 between `LoanAmount` and the three attributes `ApplicantIncome`, `CoapplicantIncome` and `Loan_Amount_Term`. Although, the most significant correlation is between `LoanAmount` and `ApplicantIncome`. That is, applicants with low income tend to request lower loan amounts than applicants with higher income according to the examples in the dataset.\n",
    "\n",
    "We can further examine the pairwise distribution between `LoanAmount`, `ApplicantIncome`, `CoapplicantIncome` and `Loan_Amount_Term` a **pair plot**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# note that by applying the method dropna() we drop all rows with missing values \n",
    "sns.pairplot(data=df[['LoanAmount','ApplicantIncome','CoapplicantIncome','Loan_Amount_Term']].dropna())\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A pair plot can be overwhelming at first, but it can also let us quickly identify interesting trends which can be examined further."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### B.2. Quick Examination of Categorical Attributes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the categorical (non-numerical) attributes (e.g. `Property_Area`, `Gender`, `Education`, etc.), we can look at the count of each value/category (i.e. a frequency table) to understand how well each category is represented in the dataset.\n",
    "\n",
    "`dfname['column_name']` is a basic indexing technique to acess a particular column of the dataframe.\n",
    "\n",
    "For example, for attribute `Gender` the frequency table can be printed by the command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df['Gender'].value_counts()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that the number of males in the dataset is more than three times larger than the number of females. We can also check the total count of values in column `Gender` with the command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df['Gender'].count()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That is, there are 13 missing values in column `Gender`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## C. Distribution Analysis for Numerical Atributes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we are familiar with the basic dataset characteristics, let's look closer at the distribution of some numerical attributes. \n",
    "\n",
    "Let's take, for example, the numerical attributes `ApplicantIncome` and `LoanAmount`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's start by plotting a histogram of ApplicantIncome using the command:\n",
    "\n",
    "df['ApplicantIncome'].hist(bins=50)\n",
    "plt.xlabel('Applicant Income')\n",
    "plt.ylabel('Number of Applicants')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the histogram above, we observe that there are few extreme values of `ApplicantIncome`. The use of 50 bins allows to depict the distribution clearly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we look at boxp lots to understand the distribution better."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.boxplot(column='ApplicantIncome')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The box plot above confirms the presence of a lot of outliers/extreme values. This may be attributed to the income disparity in the society. Part of this can be due to the fact that we are looking at people with different education levels. Let us segregate them by `Education`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Boxp lots of ApplicantIncome, grouped by the categorical attribute Education\n",
    "df.boxplot(column='ApplicantIncome', by='Education')\n",
    "plt.title('Box plot of ApplicantIncome grouped by Education')\n",
    "plt.suptitle(\"\") # get rid of the automatic 'Box plot grouped by group_by_column_name' title\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that there is no substantial difference between the mean income of graduates and non-graduates. But there are a higher number of graduates with very high incomes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Next, we examine a histogram of the numerical attribute loanAmount\n",
    "\n",
    "df['LoanAmount'].hist(bins=50)\n",
    "plt.xlabel(\"Loan Amount\")\n",
    "plt.ylabel('Number of Applicants')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Boxp lot of LoanAmount\n",
    "df.boxplot(column='LoanAmount')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, there are some extreme values. Clearly, both `ApplicantIncome` and `LoanAmount` require some amount of data munging. `LoanAmount` has missing values as well as many extreme values, while `ApplicantIncome` has a few extreme values, which demand deeper understanding. We will take this up in the next lab exercise."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, let's examine the relationship between `ApplicantIncome` and `LoanAmount` with a scatter plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.plot.scatter(x='ApplicantIncome', y='LoanAmount')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Based on this scatter plot we can say that LoanAmount probably tends to grow linearly as ApplicantIncome grows. We can see the trend clearer with `Seaborn's regplot`, which draws a line that best fits the relation between two numeric attributes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.regplot(data=df, x='ApplicantIncome', y='LoanAmount')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This regplot suggests that typically LoanAmount is 100 times lower than ApplicantIncome."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## D. Box Plot Variations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we will use the `Seaborn` module to demonstrate a few variations or alternatives to box plots."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#First, let's draw a seaborn-style box plot for LoanAmount\n",
    "\n",
    "sns.boxplot(x=df['LoanAmount'], orient='v', width=0.2)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Violin plot for LoanAmount\n",
    "\n",
    "sns.violinplot(x=df['LoanAmount'], orient='v', width=0.5)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Strip plot for LoanAmount\n",
    "\n",
    "sns.stripplot(x=df['LoanAmount'], jitter=True, orient='v')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Swar mplot for LoanAmount\n",
    "\n",
    "sns.swarmplot(x=df['LoanAmount'], orient='v')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's redraw the box plots of `ApplicantIncome` grouped by the categorical attribute `Education` as violin plots."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.violinplot(data=df, x='Education', y='ApplicantIncome')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can split each violin plot by a binary attribute. For example, we can add `Gender` to the plot above as follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.violinplot(data=df, x='Education', y='ApplicantIncome', hue='Gender', split=True)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The plot above suggest that male graduates in our dataset have more even distribution of income than female graduates, while the opposite is true for applicants who are not graduates."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## E. Distribution Analysis for Categorical Attributes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As noted above `Credit_History` is rather a categorical attribute with two categories/values:\n",
    "- 0 - the applicant has no credic history\n",
    "- 1 - the applicant has a credit history\n",
    "\n",
    "Let's look at the chances of getting a loan based on credit history. That is, let's examine the relation between atributes `Credit_History` and `Loan_Status`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, let's take a look at the frequency table for attribute `Credit_History`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "frequency_table = df['Credit_History'].value_counts(ascending=True)\n",
    "print('Frequency Table for Credit History:') \n",
    "print(frequency_table)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, build a pivot table for `Loan_Status` and `Credit_History`, i.e. find the mean `Loan_Status` for both `Credit_History`=0 and `Credit_History`=1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pivot_table_LC = df.pivot_table(values='Loan_Status',\n",
    "                                index='Credit_History',\n",
    "                                aggfunc=lambda x: x.map({'Y':1, 'N':0}).mean()) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here the values of `Loan_Status` are grouped by the index `Credit_History`, and the aggfunc is applied to each group. In the example above aggfunc is `mean()`, assuming the Y and N values in column `Loan_Status` are replaced by 1s and 0s, respectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print pivot table\n",
    "print(pivot_table_LC)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The pivot table can be interprted as follows. The probability for getting a loan is 0.078652 (i.e. ~8%) for applicants with no credit history, and 0.795789 (i.e. ~80%) for applicants with a credit history."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's plot `Credit_History` and the probability of getting a loan based on `Credit_History`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the frequency table for Credit_History\n",
    "frequency_table.plot(kind='bar')\n",
    "plt.xlabel('Credit History')\n",
    "plt.ylabel('Number of Applicants')\n",
    "plt.title('Applicants by Credit History')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot pivot table\n",
    "pivot_table_LC.plot(kind='bar')\n",
    "plt.xlabel('Credit History')\n",
    "plt.ylabel('Probability of Getting a Loan')\n",
    "plt.title('Probability of Getting a Loan by Credit History')\n",
    "plt.legend().set_visible(False) # we don't need the default legend"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternately, the two plots above can be combined in a stacked chart by plotting the output of with Pandas' `crosstab()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "crosstab_CL = pd.crosstab(df['Credit_History'], df['Loan_Status'])\n",
    "crosstab_CL.plot(kind='bar', stacked=True, color=['red', 'blue'])\n",
    "plt.ylabel('Number of Applicants')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Furthermore, we can stackchart `Credit_History` and `Gender` against `Loan_Status`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "crosstab_CGL = pd.crosstab([df['Credit_History'], df['Gender']], df['Loan_Status'])\n",
    "crosstab_CGL.plot(kind='bar', stacked=True, color=['red', 'blue'], grid=False)\n",
    "plt.ylabel('Number of Applicants')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The plot above suggests that gender does not play a significant role when taking a decision for granting a loan. However, it is hard to say which gender is more likely to have a loan granted when `Credit_History`=1, for example. To see this clearer, we can build a pivot table with the mean values of `Loan_Status` for each combination of values of `Credit_History` and `Gender` and then visualise this pivot table as a heat map."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pivot_table_LCG = df.pivot_table(values='Loan_Status', \n",
    "                           index='Credit_History', \n",
    "                           columns='Gender',\n",
    "                           aggfunc=lambda x: x.map({'Y':1, 'N':0}).mean())\n",
    "sns.heatmap(pivot_table_LCG, annot=True)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see now that a male with credit history is slightly more likely to have their loan granted than a female with credit history 1. However, we should not rush to make strong conclusions from this observation yet, because we have not examined how the numerical attributes (e.g., `LoanAmmount` and `ApplicantIncome`) relate to `Gender` and `Loan_Status`."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
